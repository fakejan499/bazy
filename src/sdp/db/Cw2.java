package sdp.db;

import java.sql.*;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicLong;

public class Cw2 {
    private static String url = "jdbc:postgresql://localhost:5432/Test";
    private static String user = "postgres";
    private static String password = "zaq1@WSX";
    private static AtomicLong atomicLong = new AtomicLong();

    public static void main(String[] args) throws ClassNotFoundException {
        usersWithoutIndex();
        /*
                Inserty trwaly 3611ms
                Selecty trwaly 13125ms
         */

        usersWithIndex();
//        Inserty trwaly 3689ms
//        Selecty trwaly 10342ms

    }

    private static void usersWithoutIndex() {
        try (
                Connection c = DriverManager.getConnection(url, user, password);
                Statement s = c.createStatement();
        ) {
            s.executeUpdate("DELETE FROM USERS");

            long msBeforeInsert = Calendar.getInstance().getTimeInMillis();
            for (int i = 0; i < 10000; i++) {
                Long l = atomicLong.getAndIncrement();
                s.executeUpdate("INSERT INTO USERS (name) VALUES ('" + String.valueOf(l) + "')");
            }
            long msAfterInsert = Calendar.getInstance().getTimeInMillis();
            System.out.println("Inserty trwaly " + String.valueOf(msAfterInsert - msBeforeInsert) + "ms");

            long msBeforeSelect = Calendar.getInstance().getTimeInMillis();
            for (int i = 0; i < 10000; i++) {
                s.executeQuery("SELECT * FROM USERS WHERE name LIKE '41%s'");
            }
            long msAfterSelect = Calendar.getInstance().getTimeInMillis();
            System.out.println("Inserty trwaly " + String.valueOf(msAfterSelect - msBeforeSelect) + "ms");


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void usersWithIndex() {
        try (
                Connection c = DriverManager.getConnection(url, user, password);
                Statement s = c.createStatement();
        ) {
            s.executeUpdate("DELETE FROM USERS_WITH_INDEX");

            long msBeforeInsert = Calendar.getInstance().getTimeInMillis();
            for (int i = 0; i < 10000; i++) {
                Long l = atomicLong.getAndIncrement();
                s.executeUpdate("INSERT INTO USERS_WITH_INDEX (name) VALUES ('" + String.valueOf(l) + "')");
            }
            long msAfterInsert = Calendar.getInstance().getTimeInMillis();
            System.out.println("Inserty trwaly " + String.valueOf(msAfterInsert - msBeforeInsert) + "ms");

            long msBeforeSelect = Calendar.getInstance().getTimeInMillis();
            for (int i = 0; i < 10000; i++) {
                s.executeQuery("SELECT * FROM USERS_WITH_INDEX WHERE name LIKE '41%s'");
            }
            long msAfterSelect = Calendar.getInstance().getTimeInMillis();
            System.out.println("Inserty trwaly " + String.valueOf(msAfterSelect - msBeforeSelect) + "ms");


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
