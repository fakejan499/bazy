package sdp.db;

import java.sql.*;

public class Cw1 {
    private static String url = "jdbc:postgresql://localhost:5432/Test";
    private static String user = "postgres";
    private static String password = "zaq1@WSX";

    public static void main(String[] args) {
        try (
                Connection c = DriverManager.getConnection(url, user, password);
                Statement s = c.createStatement();
        ) {
            s.executeUpdate("INSERT INTO USERS (name) VALUES ('Jan')");
            s.executeUpdate("INSERT INTO USERS (name) VALUES ('Mietek')");
            s.executeUpdate("INSERT INTO USERS (name) VALUES ('Kamil')");

            printUsers(s);

            s.executeUpdate("DELETE FROM USERS WHERE name = 'Kamil'");

            printUsers(s);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    private static void printUsers(Statement s) throws SQLException {
        ResultSet result = s.executeQuery("SELECT * FROM USERS");
        while (result.next()) {
            System.out.println("id: " + result.getString("id") + ", name: " + result.getString("name"));
        }
    }
}
